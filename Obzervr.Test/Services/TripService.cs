﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using NetTopologySuite;
using NetTopologySuite.Geometries;
using Obzervr.Test.Data;
using Obzervr.Test.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Obzervr.Test.Services {
    public class TripService : ITripService {
        readonly int gridSize;
        readonly DbContextOptions<ObzervrContext> dbContextOptions;
        public TripService(int gridSize, DbContextOptions<ObzervrContext> dbContextOptions) {
            this.gridSize = gridSize;
            this.dbContextOptions = dbContextOptions;
        }
        public async Task<BoundGeoSet<Cluster>> GetBoundClusterSetAsync(DateTime from, DateTime to, double latFrom, double latTo, double lngFrom, double lngTo) {
            var result = new BoundGeoSet<Cluster>() {
                LatFrom = latFrom,
                LatTo = latTo,
                LngFrom = lngFrom,
                LngTo = lngTo,
                Points = new List<Cluster>()
            };
            var clusterTasks = new Dictionary<ObzervrContext, Task<Cluster>>();
            var gridBoxes = GetGridBoxes(latFrom, latTo, lngFrom, lngTo);
            foreach(var box in gridBoxes) {
                var context = new ObzervrContext(dbContextOptions);
                ((DiagnosticListener)context.GetService<DiagnosticSource>()).SubscribeWithAdapter(new OptionRecompileCommandListener());
                clusterTasks.Add(context, context.Trips
                    .Where(t => t.PickupDatetime >= from && t.PickupDatetime <= to &&
                        t.PickupLat >= box.LatFrom && t.PickupLat < box.LatTo && t.PickupLng >= box.LngFrom && t.PickupLng < box.LngTo)
                    .GroupBy(t => 1)
                    .Select(tg => new Cluster() {
                        Lat = tg.Sum(t => t.PickupLat) / tg.Count(),
                        Lng = tg.Sum(t => t.PickupLng) / tg.Count(),
                        Count = tg.Count()
                    })
                    .AsNoTracking()
                    .FirstOrDefaultAsync());
            }
            foreach(var contextPair in clusterTasks) {
                var cluster = await contextPair.Value;
                if(cluster.Count > 0) {
                    result.Points.Add(cluster);
                }
                contextPair.Key.Dispose();
            }
            return result;
        }
        public async Task<BoundGeoSet<Marker>> GetBoundMarkerSetAsync(DateTime from, DateTime to, double latFrom, double latTo, double lngFrom, double lngTo) {
            var result = new BoundGeoSet<Marker>() {
                LatFrom = latFrom,
                LatTo = latTo,
                LngFrom = lngFrom,
                LngTo = lngTo
            };
            using(var context = new ObzervrContext(dbContextOptions)) {
                ((DiagnosticListener)context.GetService<DiagnosticSource>()).SubscribeWithAdapter(new OptionRecompileCommandListener());
                var box = new GeoBox() {
                    LatFrom = latFrom,
                    LatTo = latTo,
                    LngFrom = lngFrom,
                    LngTo = lngTo
                };
                result.Points = await context.Trips
                    .Where(t => t.PickupDatetime >= from && t.PickupDatetime <= to &&
                        t.PickupLat >= box.LatFrom && t.PickupLat < box.LatTo && t.PickupLng >= box.LngFrom && t.PickupLng < box.LngTo)
                    .Select(t => new Marker() {
                        Lat = t.PickupLat,
                        Lng = t.PickupLng,
                        Passengers = t.Passengers,
                        PickupDatetime = t.PickupDatetime
                    })
                    .AsNoTracking()
                    .ToListAsync();
            }
            return result;
        }
        IEnumerable<GeoBox> GetGridBoxes(double latFrom, double latTo, double lngFrom, double lngTo) {
            var latStep = (latTo - latFrom) / gridSize;
            var lngStep = (lngTo - lngFrom) / gridSize;
            for(var i = 0; i < gridSize; ++i) {
                for(var j = 0; j < gridSize; ++j) {
                    var gridLatFrom = latFrom + j * latStep;
                    var gridLatTo = gridLatFrom + latStep;
                    var gridLngFrom = lngFrom + i * lngStep;
                    var gridLngTo = gridLngFrom + lngStep;
                    yield return new GeoBox() {
                        LatFrom = gridLatFrom,
                        LatTo = gridLatTo,
                        LngFrom = gridLngFrom,
                        LngTo = gridLngTo
                    };
                }
            }
        }
    }
}