﻿using Obzervr.Test.Models;
using System;
using System.Threading.Tasks;

namespace Obzervr.Test.Services {
    public interface ITripService {
        Task<BoundGeoSet<Cluster>> GetBoundClusterSetAsync(DateTime from, DateTime to, double latFrom, double latTo, double lngFrom, double lngTo);
        Task<BoundGeoSet<Marker>> GetBoundMarkerSetAsync(DateTime from, DateTime to, double latFrom, double latTo, double lngFrom, double lngTo);
    }
}
