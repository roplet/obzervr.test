﻿using System;

namespace Obzervr.Test.Models {
    public class Marker : GeoPoint {
        public byte Passengers { get; set; }
        public DateTime PickupDatetime { get; set; }
    }
}
