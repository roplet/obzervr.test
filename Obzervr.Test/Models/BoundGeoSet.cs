﻿using System.Collections.Generic;

namespace Obzervr.Test.Models {
    public class BoundGeoSet<T> : GeoBox where T : GeoPoint {
        public List<T> Points { get; set; }
    }
}
