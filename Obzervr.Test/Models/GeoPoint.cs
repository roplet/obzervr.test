﻿
namespace Obzervr.Test.Models {
    public class GeoPoint {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
