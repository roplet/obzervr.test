﻿using NetTopologySuite.Geometries;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Obzervr.Test.Models {
    [Table("Trips")]
    public partial class Trips {
        [Key]
        public int Id { get; set; }
        public byte Vendor { get; set; }
        public DateTime PickupDatetime { get; set; }
        public DateTime DropoffDatetime { get; set; }
        public byte Passengers { get; set; }
        public double TripDistance { get; set; }
        public byte RateCode { get; set; }
        public byte PaymentType { get; set; }
        public double Fare { get; set; }
        public double Extra { get; set; }
        public double MtaTax { get; set; }
        public double Tip { get; set; }
        public double Tolls { get; set; }
        public double ImprovementSurcharge { get; set; }
        public double Total { get; set; }
        public bool StoreAndForward { get; set; }
        public double PickupLat { get; set; }
        public double PickupLng { get; set; }
        public double DropoffLat { get; set; }
        public double DropoffLng { get; set; }
        public Geometry Pickup { get; set; }
        public Geometry Dropoff { get; set; }
    }
}