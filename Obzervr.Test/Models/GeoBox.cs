﻿
public class GeoBox {
    public double LatFrom { get; set; }
    public double LatTo { get; set; }
    public double LngFrom { get; set; }
    public double LngTo { get; set; }
}