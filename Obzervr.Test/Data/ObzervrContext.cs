﻿using Microsoft.EntityFrameworkCore;
using Obzervr.Test.Models;

namespace Obzervr.Test.Data {
    public partial class ObzervrContext : DbContext {
        public ObzervrContext() { }

        public ObzervrContext(DbContextOptions<ObzervrContext> options) : base(options) {
            Database.SetCommandTimeout(60);
        }

        public virtual DbSet<Trips> Trips { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
