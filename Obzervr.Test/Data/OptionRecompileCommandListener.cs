﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.DiagnosticAdapter;
using System;
using System.Data;
using System.Data.Common;

namespace Obzervr.Test.Data {
    public class OptionRecompileCommandListener {
        [DiagnosticName("Microsoft.EntityFrameworkCore.Database.Command.CommandExecuting")]
        public void OnCommandExecuting(DbCommand command, DbCommandMethod executeMethod, Guid commandId, Guid connectionId, bool async, DateTimeOffset startTime) {
            if(command.CommandType == CommandType.Text && command is SqlCommand && !command.CommandText.Contains("option(recompile)")) {
                command.CommandText += " option(recompile)";
            }
        }
        [DiagnosticName("Microsoft.EntityFrameworkCore.Database.Command.CommandExecuted")]
        public void OnCommandExecuted(object result, bool async) { }
        [DiagnosticName("Microsoft.EntityFrameworkCore.Database.Command.CommandError")]
        public void OnCommandError(Exception exception, bool async) { }
    }
}