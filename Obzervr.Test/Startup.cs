using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Obzervr.Test.Data;
using Obzervr.Test.Services;
using System;
using System.IO;

namespace Obzervr.Test {
    public class Startup {
        static readonly string obzervrConnectionKey = "ObzervConnection";
        readonly IConfiguration configuration;
        public Startup(IConfiguration configuration) {
            this.configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services) {
            services.AddControllers();
            services.AddRazorPages();
            var obzervrConnectionString = configuration.GetConnectionString(obzervrConnectionKey);
            services.AddDbContext<ObzervrContext>(options => options.UseSqlServer(obzervrConnectionString, b => b.UseNetTopologySuite()));
            services.AddScoped<ITripService>(sp => new TripService(5, sp.GetRequiredService<DbContextOptions<ObzervrContext>>()));
            services.AddSwaggerGen(o => {
                var xmlComments = Path.Combine(AppContext.BaseDirectory, "Obzervr.Test.xml");
                o.IncludeXmlComments(xmlComments);
            });
            services.AddCors(options => {
                options.AddPolicy("DefaultPolicy",
                    builder => {
                        builder.WithOrigins("*");
                    }
                );
            });
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if(env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(o => { o.SwaggerEndpoint("/swagger/v1/swagger.json", "Obzervr.Test API v1"); });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
            });
        }
    }
}
