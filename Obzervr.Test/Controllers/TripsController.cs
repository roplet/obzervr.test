﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Obzervr.Test.Models;
using Obzervr.Test.Services;
using System;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Obzervr.Test.Controllers {
    [EnableCors("DefaultPolicy")]
    [ApiController]
    [Route("api/[controller]")]
    public class TripsController : ControllerBase {
        readonly ILogger<TripsController> logger;
        readonly ITripService tripService;
        public TripsController(ILogger<TripsController> logger, ITripService tripService) {
            this.logger = logger;
            this.tripService = tripService;
        }
        /// <summary>
        /// Retrieves clusters of trips made within a particular area and a date/time range.
        /// </summary>
        /// <param name="from">Date/time after which the trips were made.</param>
        /// <param name="to">Date/time before which the trips were made.</param>
        /// <param name="latFrom">Minimum bounding latitude of the area.</param>
        /// <param name="latTo">Maximum bounding latitude of the area.</param>
        /// <param name="lngFrom">Minimum bounding longtitude of the area.</param>
        /// <param name="lngTo">Maximum bounding longtitude of the area.</param>
        /// <returns>Trip clusters alond with the bounding area.</returns>
        [HttpGet]
        [Route("BoundCluster")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces(MediaTypeNames.Application.Json)]
        public Task<BoundGeoSet<Cluster>> GetClusters(DateTime from, DateTime to, double latFrom, double latTo, double lngFrom, double lngTo) {
            return tripService.GetBoundClusterSetAsync(from, to, latFrom, latTo, lngFrom, lngTo);
        }
        /// <summary>
        /// Retrieves markers for trips made within a particular area and a date/time range.
        /// </summary>
        /// <param name="from">Date/time after which the trips were made.</param>
        /// <param name="to">Date/time before which the trips were made.</param>
        /// <param name="latFrom">Minimum bounding latitude of the area.</param>
        /// <param name="latTo">Maximum bounding latitude of the area.</param>
        /// <param name="lngFrom">Minimum bounding longtitude of the area.</param>
        /// <param name="lngTo">Maximum bounding longtitude of the area.</param>
        /// <returns>Trip markers alond with the bounding area.</returns>
        [HttpGet]
        [Route("BoundMarker")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces(MediaTypeNames.Application.Json)]
        public Task<BoundGeoSet<Marker>> GetMarkers(DateTime from, DateTime to, double latFrom, double latTo, double lngFrom, double lngTo) {
            return tripService.GetBoundMarkerSetAsync(from, to, latFrom, latTo, lngFrom, lngTo);
        }
    }
}