import { GeoPoint } from "./geopoint";

export class Marker extends GeoPoint {
  passengers: number;
  pickupDatetime: string;
}
