import { GeoPoint } from "./geopoint";

export class BoundGeoSet<T extends GeoPoint> {
  latFrom: number;
  latTo: number;
  lngFrom: number;
  lngTo: number;
  points: T[];
}
