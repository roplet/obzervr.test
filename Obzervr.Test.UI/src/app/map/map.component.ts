import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Cluster } from '../models/cluster';
import { Marker } from '../models/marker';
import { BoundGeoSet } from '../models/boundgeoset';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
})

export class MapComponent implements OnInit {
  options: any;
  overlays: any[];
  map: google.maps.Map;
  isLoading: boolean = false;
  dateFrom: Date = new Date(2015, 0, 1, 13);
  dateTo: Date = new Date(2015, 0, 1, 14);
  dateMin: Date;
  dateMax: Date;
  constructor(private http: HttpClient) { }
  setMap(event) {
    this.map = event.map;
    this.map.addListener('idle', this.fetchClustersAndUpdateOverlay.bind(this));
  }
  ngOnInit() {
    this.options = {
      center: { lat: 40.730610, lng: -73.935242 },
      zoom: 10,
      minZoom: 10,
      streetViewControl: false,
      mapTypeControl: false,
      mapTypeControlOptions: {
        mapTypeIds: ['roadmap']
      }
    };
    this.overlays = [];
    this.dateMin = new Date(2015, 0, 1, 0, 0);
    this.dateMax = new Date(2015, 0, 31, 23, 59);
  }
  formatDateForApi(d: Date): string {
    const year = d.getFullYear(),
      month = (d.getMonth() + 1).toString().padStart(2, '0'),
      date = d.getDate().toString().padStart(2, '0'),
      hours = d.getHours().toString().padStart(2, '0'),
      minutes = d.getMinutes().toString().padStart(2, '0');
    return `${year}-${month}-${date}T${hours}:${minutes}`;
  }
  getGeoDateParameterString(baseUrl: string, from: Date, to: Date, latFrom: number, latTo: number, lngFrom: number, lngTo: number): string {
    return `${baseUrl}?latFrom=${latFrom}&latTo=${latTo}&lngFrom=${lngFrom}&lngTo=${lngTo}&from=${this.formatDateForApi(from)}&to=${this.formatDateForApi(to)}`;
  }
  fetchClusters(baseUrl: string, from: Date, to: Date, latFrom: number, latTo: number, lngFrom: number, lngTo: number): Observable<BoundGeoSet<Cluster>> {
    return this.http.get<BoundGeoSet<Cluster>>(this.getGeoDateParameterString(baseUrl, from, to, latFrom, latTo, lngFrom, lngTo));
  }
  fetchMarkers(baseUrl: string, from: Date, to: Date, latFrom: number, latTo: number, lngFrom: number, lngTo: number): Observable<BoundGeoSet<Marker>> {
    return this.http.get<BoundGeoSet<Marker>>(this.getGeoDateParameterString(baseUrl, from, to, latFrom, latTo, lngFrom, lngTo));
  }
  fetchClustersAndUpdateOverlay() {
    this.isLoading = true;
    const bounds = this.map.getBounds(),
      ne = bounds.getNorthEast(),
      sw = bounds.getSouthWest();
    this.fetchClusters(environment.getClustersUrl, this.dateFrom, this.dateTo, sw.lat(), ne.lat(), sw.lng(), ne.lng()).subscribe(result => this.updateOverlay(result));
  }
  fetchMarkersAndUpdateOverlay() {
    this.isLoading = true;
    const bounds = this.map.getBounds(),
      ne = bounds.getNorthEast(),
      sw = bounds.getSouthWest();
    this.fetchMarkers(environment.getMarkersUrl, this.dateFrom, this.dateTo, sw.lat(), ne.lat(), sw.lng(), ne.lng()).subscribe(result => this.updateOverlayWithMarkers(result));
  }
  getClusterCaption(cluster: Cluster): string {
    let clusterText = cluster.count.toString();
    if (cluster.count >= 1e6) {
      clusterText = (Math.round(cluster.count / 1e6 * 100) / 100).toFixed(1) + 'M';
    } else if (cluster.count >= 1e3) {
      clusterText = (Math.round(cluster.count / 1e3 * 100) / 100).toFixed(1) + 'K';
    }
    return clusterText;
  }
  getMarkerCaption(marker: Marker): string {
    return marker.passengers.toString();
  }
  boundSetMatchesBounds(boundSet: BoundGeoSet<Cluster> | BoundGeoSet<Marker>, bounds: google.maps.LatLngBounds): boolean {
    const ne = bounds.getNorthEast(),
      sw = bounds.getSouthWest();
    return boundSet.latFrom === sw.lat()
      && boundSet.latTo === ne.lat()
      && boundSet.lngFrom === sw.lng()
      && boundSet.lngTo == ne.lng();
  }
  updateOverlay(clusterSet: BoundGeoSet<Cluster>) {
    if (this.boundSetMatchesBounds(clusterSet, this.map.getBounds())) {
      let count = 0;
      for (const cluster of clusterSet.points) {
        count += cluster.count;
      }
      if (count <= 25 || this.map.getZoom() === 22) {
        this.fetchMarkersAndUpdateOverlay();
      } else {
        this.updateOverlayWithClusters(clusterSet);
      }
    }
  }
  updateOverlayWithClusters(clusterSet: BoundGeoSet<Cluster>) {
    if (this.boundSetMatchesBounds(clusterSet, this.map.getBounds())) {
      this.overlays.length = 0;
      for (const cluster of clusterSet.points) {
        const circleScale = Math.pow(Math.log(cluster.count), 2),
          circleScaleNormalized = circleScale < 15 ? 15 : circleScale;
        this.overlays.push(
          new google.maps.Marker({
            position: new google.maps.LatLng(cluster.lat, cluster.lng),
            icon: {
              path: google.maps.SymbolPath.CIRCLE,
              fillOpacity: 0.5,
              fillColor: '#ff0000',
              strokeOpacity: 1.0,
              strokeColor: '#fff000',
              strokeWeight: 3.0,
              scale: circleScaleNormalized
            }
          })
        );
        this.overlays.push(
          new google.maps.Marker({
            position: { lat: cluster.lat, lng: cluster.lng },
            icon: 'assets/empty.png',
            label: {
              color: '#000000',
              fontWeight: 'bold',
              text: this.getClusterCaption(cluster),
              fontSize: '15px',
            }
          })
        );
      }
      this.isLoading = false;
    }
  }
  updateOverlayWithMarkers(markerSet: BoundGeoSet<Marker>) {
    if (this.boundSetMatchesBounds(markerSet, this.map.getBounds())) {
      this.overlays.length = 0;
      for (const marker of markerSet.points) {
        this.overlays.push(
          new google.maps.Marker({
            position: { lat: marker.lat, lng: marker.lng },
            label: {
              color: '#000000',
              fontWeight: 'bold',
              text: this.getMarkerCaption(marker),
              fontSize: '15px',
            }
          })
        );
      }
      this.isLoading = false;
    }
  }
}
