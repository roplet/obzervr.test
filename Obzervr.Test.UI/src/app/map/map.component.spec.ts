import { TestBed, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MapComponent } from './map.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { GMapModule } from 'primeng/gmap'
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { CalendarModule } from 'primeng/calendar';
import { environment } from 'src/environments/environment';
import { Cluster } from '../models/cluster';
import { Marker } from '../models/marker';

describe('MapComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MapComponent
      ],
      imports: [
        FormsModule,
        HttpClientModule,
        GMapModule,
        ProgressSpinnerModule,
        CalendarModule
      ]
    }).compileComponents();
  }));

  it('should fetch clusters', async(() => {
    const fixture = TestBed.createComponent(MapComponent);
    const mapComponent = <MapComponent>fixture.debugElement.componentInstance;

    const from = new Date(2015, 0, 1, 13, 0, 0,),
      to = new Date(2015, 0, 7, 14, 0, 0),
      latFrom = 40.33944301640769,
      latTo = 41.006035327480255,
      lngFrom = -73.41088434755417,
      lngTo = -72.03759333192917;

    mapComponent.fetchClusters(environment.getClustersUrl, from, to, latFrom, latTo, lngFrom, lngTo).subscribe((clusters) => {
      expect(clusters.points.length).toEqual(2);
      expect(clusters.latFrom).toEqual(latFrom);
      expect(clusters.latTo).toEqual(latTo);
      expect(clusters.lngFrom).toEqual(lngFrom);
      expect(clusters.lngTo).toEqual(lngTo);

      const expectedClusterA = <Cluster>{
        lat: 40.82962646484375,
        lng: -73.3643569946289,
        count: 5
      }, expectedClusterB = <Cluster>{
        lat: 40.80038642883301,
        lng: -73.1090202331543,
        count: 2
      };

      expect(clusters.points).toEqual(jasmine.arrayContaining([expectedClusterA, expectedClusterB]));
    });
  }));

  it('should fetch markers', async(() => {
    const fixture = TestBed.createComponent(MapComponent);
    const mapComponent = <MapComponent>fixture.debugElement.componentInstance;

    const from = new Date(2015, 0, 1, 13, 0, 0,),
      to = new Date(2015, 0, 7, 14, 0, 0),
      latFrom = 40.69841653667833,
      latTo = 40.864794397755865,
      lngFrom = -73.25726593825938,
      lngTo = -72.91394318435313;

    mapComponent.fetchMarkers(environment.getMarkersUrl, from, to, latFrom, latTo, lngFrom, lngTo).subscribe((markers) => {
      expect(markers.points.length).toEqual(2);
      expect(markers.latFrom).toEqual(latFrom);
      expect(markers.latTo).toEqual(latTo);
      expect(markers.lngFrom).toEqual(lngFrom);
      expect(markers.lngTo).toEqual(lngTo);

      const expectedMarkerA = <Marker>{
        lat: 40.787750244140625,
        lng: -73.10249328613281,
        passengers: 1,
        pickupDatetime: '2015-01-04T12:17:43'
      }, expectedMarkerB = <Marker>{
        lat: 40.81302261352539,
        lng: -73.11554718017578,
        passengers: 1,
        pickupDatetime: '2015-01-04T09:05:14'
      };

      expect(markers.points).toEqual(jasmine.arrayContaining([expectedMarkerA, expectedMarkerB]));
    });
  }));
});