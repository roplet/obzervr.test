﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Obzervr.Test.Controllers;
using Obzervr.Test.Data;
using Obzervr.Test.Services;
using System.Collections.Generic;

namespace Obzervr.Test.Tests.Integration {
    public class IntegrationTestFixture {
        static readonly string obzervrConnectionKey = "ObzervConnection";
        static readonly Dictionary<string, string> testConfiguration = new Dictionary<string, string> {
            { $"ConnectionStrings:{obzervrConnectionKey}", "Data Source=(local);Initial Catalog=obzervr;Integrated Security=SSPI;" },
        };
        protected readonly IConfigurationRoot configurationRoot;
        protected readonly ServiceProvider serviceProvider;
        public IntegrationTestFixture() {
            configurationRoot = new ConfigurationBuilder()
                .AddInMemoryCollection(testConfiguration)
                .Build();
            var services = new ServiceCollection();
            var obzervrConnectionString = configurationRoot.GetConnectionString(obzervrConnectionKey);
            services.AddDbContext<ObzervrContext>(options => options.UseSqlServer(obzervrConnectionString, b => b.UseNetTopologySuite()));
            services.AddScoped<ITripService>(sp => new TripService(5, sp.GetRequiredService<DbContextOptions<ObzervrContext>>()));
            services.AddScoped<TripsController>();
            services.AddScoped<ILogger<TripsController>>(f => NullLogger<TripsController>.Instance);
            serviceProvider = services.BuildServiceProvider();
        }
    }
}
