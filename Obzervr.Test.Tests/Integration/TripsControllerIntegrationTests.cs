﻿using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Obzervr.Test.Controllers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Obzervr.Test.Tests.Integration {
    [TestFixture]
    public class TripsControllerIntegrationTests : IntegrationTestFixture {
        [Test]
        public async Task GetClusters() {
            DateTime from = new DateTime(2015, 01, 01, 13, 00, 00),
                to = new DateTime(2015, 01, 07, 14, 00, 00);

            double latFrom = 40.33944301640769,
                latTo = 41.006035327480255,
                lngFrom = -73.41088434755417,
                lngTo = -72.03759333192917;

            var testController = serviceProvider.GetRequiredService<TripsController>();
            var clusters = await testController.GetClusters(from, to, latFrom, latTo, lngFrom, lngTo);

            Assert.AreEqual(2, clusters.Points.Count);
            Assert.AreEqual(latFrom, clusters.LatFrom);
            Assert.AreEqual(latTo, clusters.LatTo);
            Assert.AreEqual(lngFrom, clusters.LngFrom);
            Assert.AreEqual(lngTo, clusters.LngTo);

            Assert.That(clusters.Points.Any(i => i.Lat == 40.82962646484375 && i.Lng == -73.3643569946289 && i.Count == 5));
            Assert.That(clusters.Points.Any(i => i.Lat == 40.80038642883301 && i.Lng == -73.1090202331543 && i.Count == 2));
        }
        [Test]
        public async Task GetMarkers() {
            DateTime from = new DateTime(2015, 01, 01, 13, 00, 00),
                to = new DateTime(2015, 01, 07, 14, 00, 00);
            double latFrom = 40.69841653667833,
                latTo = 40.864794397755865,
                lngFrom = -73.25726593825938,
                lngTo = -72.91394318435313;

            var testController = serviceProvider.GetRequiredService<TripsController>();
            var markers = await testController.GetMarkers(from, to, latFrom, latTo, lngFrom, lngTo);

            Assert.AreEqual(2, markers.Points.Count);
            Assert.AreEqual(latFrom, markers.LatFrom);
            Assert.AreEqual(latTo, markers.LatTo);
            Assert.AreEqual(lngFrom, markers.LngFrom);
            Assert.AreEqual(lngTo, markers.LngTo);

            Assert.That(markers.Points.Any(i => i.Lat == 40.787750244140625 && i.Lng == -73.10249328613281 && i.Passengers == 1 && i.PickupDatetime == new DateTime(2015, 01, 04, 12, 17, 43)));
            Assert.That(markers.Points.Any(i => i.Lat == 40.81302261352539 && i.Lng == -73.11554718017578 && i.Passengers == 1 && i.PickupDatetime == new DateTime(2015, 01, 04, 09, 05, 14)));
        }
    }
}
